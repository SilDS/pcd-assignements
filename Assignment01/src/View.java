import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class View extends JFrame implements ActionListener {

    private cellMatrix matrix;
    private Controller controller;
    private JPanel container;
    private JPanel buttons;
    private static final int BLOCK_SIZE = 5;
    private final Dimension MATRIX_DIMESION = new Dimension(1280, 1024);
    private JButton start;
    private JButton stop;
    private Label aliveCells;

    public View(cellMatrix matrix, Controller controller) {
        this.controller = controller;
        this.matrix = matrix;

        setTitle("Game of Life");
        setSize(800, 600);
        setLocationRelativeTo(null);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);

        JSplitPane mainPanel = new JSplitPane();
        buttons = new JPanel();

        start = new JButton("start");
        stop = new JButton("stop");
        aliveCells = new Label();
        this.setLabel();

        start.addActionListener(this);
        stop.addActionListener(this);
        stop.setEnabled(false);
        buttons.add(start);
        buttons.add(stop);
        buttons.add(aliveCells);

        GamePanel controllerView = new GamePanel(matrix.getRows(), matrix.getColumns());
        controllerView.setPreferredSize(MATRIX_DIMESION);
        JScrollPane scrollPane = new JScrollPane(controllerView);

        mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));
        getContentPane().add(mainPanel);
        mainPanel.setTopComponent(scrollPane);
        mainPanel.setBottomComponent(buttons);
    }


    public void setLabel(){
        String alive=matrix.getAliveCells();
        aliveCells.setText(alive);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("start")) {
            start.setEnabled(false);
            stop.setEnabled(true);
            controller.setFlag(true);
            controller.setView(this);
            controller.start();
        }
        if (e.getActionCommand().equals("stop")) {
            controller.setFlag(false);
        }
    }

    protected class GamePanel extends JPanel {

        private int dx;
        private int dy;

        public GamePanel(int w, int h) {

            this.dx = w;
            this.dy = h;
        }


        @Override
        public void paintComponent(Graphics g) {
            super.paintComponent(g);

            for (int i = 0; i <= dy * BLOCK_SIZE; i = i + BLOCK_SIZE) {
                g.drawLine(i, 0, i, dy * BLOCK_SIZE);
            }

            for (int j = 0; j <= dx * BLOCK_SIZE; j = j + BLOCK_SIZE) {
                g.drawLine(0, j, dx * BLOCK_SIZE, j);
            }

            for (int h = 0; h < matrix.getRows(); h++) {
                for (int k = 0; k < matrix.getColumns(); k++) {
                    g.setColor(Color.darkGray);
                    if (matrix.getCell(h, k) ) {
                        g.fillRect(h * BLOCK_SIZE, k * BLOCK_SIZE, BLOCK_SIZE, BLOCK_SIZE);
                    }

                }
            }

        }
    }




}
