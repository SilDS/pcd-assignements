package Ex1_GOL;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.Props;

import java.util.List;

public class BootActor extends AbstractActor {

    private final int width;
    private final int height;
    private final int period;
    private final int viewWidth;
    private final int viewHeight;
    private final CellGrid grid;
    private ActorRef controller;
    private ActorRef view;
    private List<ActorRef> worker;

    public BootActor(int width, int height, int rate, int viewWidth, int viewHeight) {
        this.width = width;
        this.height = height;
        this.viewWidth = viewWidth;
        this.viewHeight = viewHeight;
        this.period = 1000 / rate;

        this.grid = new CellGrid(this.width, this.height);
        this.grid.initRandom(350 * ((this.height > this.width) ? this.height : this.width));
    }

    @Override
    public void preStart() {
        // view -> width, height, cellGrid (creates the Ex1.View object)
        view = getContext().actorOf(Props.create(ViewActor.class, this.viewWidth, this.viewHeight, grid), "view");
        // controller -> period update view, actor worker, grid, view actor
        controller = getContext().actorOf(Props.create(ControllerActor.class, this.period, this.worker, this.grid, this.view), "controller");
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .build();
    }

}