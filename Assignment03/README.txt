In directory Ex1_GOL you will find another version of Game of Life developed using Akka Actors framework.
In directory Ex2_Chat you will find a distributed and decentralized chat client also developed with Akka Actors framework working with the matrix vector algorithm.
