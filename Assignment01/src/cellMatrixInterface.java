public interface cellMatrixInterface {

    void randomInit();

    void nextGeneration();

    String getAliveCells();

    int getRows();

    int getColumns();

    boolean getCell(int i, int j);

    void updateState(int initialRow, int FinalRow);
}
