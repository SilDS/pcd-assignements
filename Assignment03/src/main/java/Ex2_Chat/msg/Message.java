package Ex2_Chat.msg;

import Ex2_Chat.MatrixClock;

import java.io.Serializable;

public class Message implements Serializable {

	private String value;
	private int id;
	private MatrixClock matrix;



	public Message (String value) {
		this.value = value;
	}

	public Message(String value, int id , MatrixClock matrix) {
		this(value);
		this.id = id;
		this.matrix = matrix;
	}

	public String getMsg() {
		return value;
	}

	public int getId() {
		return this.id;
	}

	public int[][] getMatrix() {
		return this.matrix.getMatrix();
	}
}
