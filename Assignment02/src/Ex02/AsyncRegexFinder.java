package Ex02;

import io.vertx.core.Vertx;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;


public class AsyncRegexFinder {

    public static void main(String[] args) throws IOException {

        Vertx vertx = Vertx.vertx();

        String rootDir = args[0];
        Pattern regex = Pattern.compile(String.format("\\%s", args[1]));
        int depth = Integer.parseInt(args[2]);

        List<Path> files = new LinkedList<>();

        Files.walk(Paths.get(rootDir), depth)
                .filter(Files::isRegularFile)
                .collect(Collectors.toCollection(() -> files));

        final double[] matches = {0};
        final double[] matched = {0};
        final double[] percentage = {0};
        final double[] mean = {0};

        for (int i = 0; i < files.size(); i++) {
            int finalI = i;

            vertx.fileSystem().readFile(String.valueOf(files.get(i)), match -> {
                Matcher matcher = regex.matcher(match.result().toString(StandardCharsets.ISO_8859_1));
                boolean found = false;
                while (matcher.find()) {
                    matches[0]++;
                    found = true;
                }
                if (!found){
                    log("No match found in: " + files.get(finalI));
                } else {
                    matched[0]++;
                    percentage[0] = (matched[0]/files.size())*100;
                    mean[0] = matches[0]/matched[0];
                    log("Match found in: " + files.get(finalI));
                }

                log( "Total matches: " + matches[0] +
                        "\nPercentage of files with at least one match: " + percentage[0] + "%" +
                        "\nMean number of matches: " + mean[0]);
            });
        }
    }

    private static void log(String msg){
        System.out.println(msg);
    }

}