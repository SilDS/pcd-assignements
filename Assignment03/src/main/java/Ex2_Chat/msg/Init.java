package Ex2_Chat.msg;

import java.io.Serializable;

public class Init implements Serializable {

    private int id;

    public Init(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }
}
