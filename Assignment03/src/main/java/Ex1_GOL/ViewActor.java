package Ex1_GOL;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import Ex1_GOL.msg.ShowGUI;
import Ex1_GOL.msg.StartGame;
import Ex1_GOL.msg.StopGame;
import Ex1_GOL.msg.UpdateGUI;

public class ViewActor extends AbstractActor implements InputListener {

    private final int width;
    private final int height;
    private final CellGrid grid;
    private final View view;
    private ActorRef controller;

    public ViewActor(int width, int height, CellGrid grid) {
        this.width = width;
        this.height = height;
        this.grid = grid;
        this.view = new View(this.width, this.height, this.grid);
        this.view.addListener(this);
    }

    @Override
    public void started() {
        this.controller.tell(new StartGame(), getSelf());
    }

    @Override
    public void stopped() {
        this.controller.tell(new StopGame(), getSelf());
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(ShowGUI.class, msg -> {
                    this.controller = getSender();
                    this.view.setVisible(true);
                })
                .match(UpdateGUI.class, msg -> {
                    this.view.update(msg.size, msg.time);
                })
                .build();
    }
}