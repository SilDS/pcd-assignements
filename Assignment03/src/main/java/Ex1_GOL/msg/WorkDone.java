package Ex1_GOL.msg;

import java.awt.*;
import java.util.ArrayList;

public class WorkDone {

    public String type;
    public ArrayList<Point> result;

    public WorkDone(String type) {
        this.type = type;
    }

    public WorkDone(String type, ArrayList<Point> result) {
        this.type = type;
        this.result = result;
    }

}