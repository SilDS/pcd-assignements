package Ex2_Chat.msg;

import java.io.Serializable;

public class Msg implements Serializable {

    private String msg;
    private int id;

    public Msg(String msg, int id) {
        this.msg = msg;
        this.id = id;
    }

    public String getMsg() {
        return msg;
    }

    public int getId() {
        return id;
    }
}
