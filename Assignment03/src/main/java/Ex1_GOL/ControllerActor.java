package Ex1_GOL;

import Ex1_GOL.msg.*;
import akka.actor.*;
import scala.concurrent.duration.Duration;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class ControllerActor extends AbstractActor {

    private final Flag stop;
    private final int period;
    private final CellGrid grid;
    private final ActorRef view;
    private final ActorSystem system;
    private final ArrayList<Point> live;
    private int countWorkers;
    private List<ActorRef> workers;
    private long timeNano;
    private long time;

    public ControllerActor(int rate, List<ActorRef> workers, CellGrid grid, ActorRef view) {
        this.period = (int) (rate * Math.pow(10, 6));
        this.workers = workers;
        this.grid = grid;
        this.view = view;
        this.stop = new Flag();
        this.system = ActorSystem.create("system");
        this.live = new ArrayList<>();
    }

    @Override
    public void preStart() {
        // Creates and configures a workers for each row
        this.workers = new ArrayList<>();
        ActorRef up;
        ActorRef lo;
        int height = this.grid.getHeight();
        this.countWorkers = height;

        for (int i = 0; i < height; i++) {
            this.workers.add(getContext().actorOf(Props.create(Worker.class, i), "Worker-" + i));
        }

        // Setting up the initial values of each row cell
        // for the first row (0)
        up = this.workers.get(height - 1);
        lo = this.workers.get(1);
        this.workers.get(0).tell(new ConfigWorker(this.grid.getRow(0), up, lo), getSelf());

        for (int i = 1; i < (height - 1); i++) {
            up = this.workers.get(i - 1);
            lo = this.workers.get(i + 1);
            this.workers.get(i).tell(new ConfigWorker(grid.getRow(i), up, lo), getSelf());
        }

        up = this.workers.get(height - 2);
        lo = this.workers.get(0);
        this.workers.get(height - 1).tell(new ConfigWorker(this.grid.getRow(height - 1), up, lo), getSelf());
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(WorkDone.class, msg -> {
                    switch (msg.type) {
                        case "config":
                            this.countWorkers--;
                            if (this.countWorkers == 0) {
                                this.countWorkers = this.workers.size();
                                this.view.tell(new ShowGUI(), getSelf());
                            }
                            break;
                        case "turn":
                            this.countWorkers--;
                            this.live.addAll(msg.result);
                            if (this.countWorkers == 0) {
                                this.countWorkers = this.workers.size();
                                this.updateView();
                            }
                            break;
                    }
                })
                .match(StartGame.class, msg -> {
                    this.stop.reset();
                    getSelf().tell(new NextTurn(), getSelf());
                })
                .match(StopGame.class, msg -> {
                    this.stop.set();
                })
                .match(NextTurn.class, msg -> {
                    this.nextTurn();
                })
                .build();
    }

    private void nextTurn() {
        this.time = System.currentTimeMillis();
        this.timeNano = System.nanoTime();
        this.live.clear();
        // lunch the computation
        this.workers.forEach(w -> w.tell(new DoTurn(), getSelf()));
    }


    private void updateView() {
        long computeTimeNano = (System.nanoTime() - this.timeNano);
        // Update the model
        this.grid.setLive(this.live);
        this.view.tell(new UpdateGUI(this.live.size(), computeTimeNano), getSelf());
        if (!this.stop.isSet()) {
            if (computeTimeNano < period) {
                // wait until the correct frame rate
                this.system.scheduler().scheduleOnce(Duration.create(period - computeTimeNano, TimeUnit.NANOSECONDS), () -> {
                    getSelf().tell(new NextTurn(), getSelf());
                }, this.system.dispatcher());
            } else {
                getSelf().tell(new NextTurn(), getSelf());
            }
        }
    }
}