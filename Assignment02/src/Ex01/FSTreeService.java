package Ex01;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.regex.Pattern;
import java.util.stream.Collectors;


public class FSTreeService extends Thread {

    private ExecutorService executor;

    public FSTreeService(int poolSize){
        executor = Executors.newFixedThreadPool(poolSize);
    }

    public double compute(Path file, Pattern regex, int depth) throws IOException {
        long startTime = System.currentTimeMillis();
        Set<Future<Double>> searched = new HashSet<>();
        List<Path> files = new LinkedList<>();

            Files.walk(file, depth)
                    .filter(Files::isRegularFile)
                    .collect(Collectors.toCollection(() -> files));

            for (int i=0; i<files.size();i++){
                try {
                    Future<Double> res = executor.submit(new RegexFinderTask(files.get(i), regex));
                    searched.add(res);
                    log("Task submitted!");
                } catch (Exception ex){
                    ex.printStackTrace();
                }
            }

            double matches = 0;
            double percentage = 0;
            double matched = 0;
            double mean = 0;

            for (Future<Double> future: searched){
                try {
                    matches += future.get();
                    if (future.get()!=0){
                        matched++;
                        percentage = (matched/searched.size())*100;
                        mean = (matches/matched);
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            log("Total matches: " + matches +
                    "\nPercentage of files with at least one match: " + percentage + "%" +
                    "\nMean number of matches: " + mean);
            log("Time to complete: " + (System.currentTimeMillis() - startTime + " ms"));
            executor.shutdown();
            return matches;

    }

    private void log(String msg){
        System.out.println("\u001B[1m" + "[SERVICE]: " + "\u001B[0m" + msg);
    }


}