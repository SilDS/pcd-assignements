import java.util.Random;

public class cellMatrix implements cellMatrixInterface {

    private int rows;
    private int columns;
    private boolean [][] cells;
    private boolean [][] futureCells;
    private int count=0;

    public cellMatrix(int rows, int columns) {
        this.rows = rows;
        this.columns = columns;
        this.cells = new boolean[rows][columns];
        this.futureCells = new boolean[rows][columns];

    }


    @Override
    public void randomInit() {
        Random random = new Random();
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                boolean bool=random.nextBoolean();
                cells[i][j] = bool;
                if(bool==true) {count++;}

            }
        }
        futureCells =cells;
    }



    @Override
    public void nextGeneration() {
        cells = futureCells;
        this.count=0;

    }

    @Override
    public String getAliveCells(){
        if (count>0) {
            return "Alive cells: " + count;
        }else{
            return "Every cell is dead";
        }
    }

    @Override
    public int getRows() {
        return this.rows;
    }

    @Override
    public int getColumns() {
        return this.columns;
    }

    @Override
    public boolean getCell(int i, int j){
        return cells[i][j];
    }


    @Override
    public void updateState(int initialRow, int finalRow) {
        boolean[][] matrix = this.cells;

        for (int i = initialRow; i < finalRow; i++) {
            for (int j = 0; j < this.columns; j++) {
                int neighbors = 0;

                if (matrix[rowModulus(i - 1)][colModulus(j - 1)]) neighbors++;
                if (matrix[i][colModulus(j - 1)] ) neighbors++;
                if (matrix[rowModulus(i + 1)][colModulus(j - 1)]) neighbors++;
                if (matrix[rowModulus(i - 1)][j] ) neighbors++;
                if (matrix[rowModulus(i + 1)][j]) neighbors++;
                if (matrix[rowModulus(i - 1)][colModulus(j + 1)]) neighbors++;
                if (matrix[i][colModulus(j + 1)] ) neighbors++;
                if (matrix[rowModulus(i + 1)][colModulus(j + 1)] ) neighbors++;

                if (matrix[i][j] && (neighbors == 2 || neighbors == 3)) {
                    futureCells[i][j]=true;
                    count++;
                } else if (!matrix[i][j] && neighbors == 3) {
                    futureCells[i][j]=true;
                    count++;
                } else {
                    futureCells[i][j]=false;
                }
            }
        }
    }

    private int rowModulus(int index) {

        int indice = ((index % this.getColumns()) + this.getColumns()) % this.getColumns();
        return indice;
    }

    private int colModulus(int index) {

      int indice = ((index % this.getRows()) + this.getRows()) % this.getRows();
        return indice;
    }

}
