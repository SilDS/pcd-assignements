package Ex2_Chat.msg;

import java.io.Serializable;

public class ViewMsg implements Serializable {

    private String msg;

    public ViewMsg(String msg) {
        this.msg = msg;
    }

    public String getMsg() {
        return msg;
    }
}
