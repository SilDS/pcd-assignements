package Ex01;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.concurrent.Callable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexFinderTask implements Callable<Double> {

    private Pattern regex;
    private Path file;

    public RegexFinderTask(Path file, Pattern regex){
        this.file = file;
        this.regex = regex;
    }

    @Override
    public Double call() throws IOException {
        //long startTime = System.currentTimeMillis();

        double matches = 0;
        boolean found = false;
        try (BufferedReader reader = Files.newBufferedReader(file, StandardCharsets.ISO_8859_1)){
            String line = null;
            while ((line = reader.readLine())!=null){
                Matcher matcher = regex.matcher(line);
                while (matcher.find()){
                    matches++;
                    found = true;
                }
            }
        }
        log(found ? "Match found in: " + file
                : "\u001B[31m" + "No match found in: " + "\u001B[0m" + file);
        //log("Time to read the file: " + (System.currentTimeMillis() - startTime) + " ms");
        return matches;

    }

    private void log(String msg){
        System.out.println(msg);
    }
}
