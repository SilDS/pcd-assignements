package Ex1_GOL.msg;

public class GetRow {

    public final String type;
    public final boolean[] row;

    public GetRow(String type) {
        this.type = type;
        this.row = null;
    }

    public GetRow(String type, boolean[] row) {
        this.type = type;
        this.row = row;
    }

}