package Ex2_Chat;

import akka.actor.ActorSystem;
import akka.actor.Props;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

import java.io.File;

public class WhitePagesService {
    public static void main(String[] args) {

       Config config = ConfigFactory.parseFile(new File("src/main/java/Ex2_Chat/whitepages.conf"));
        ActorSystem system = ActorSystem.create("Chat-System",config);
        system.actorOf(Props.create(WhitePages.class),"white-pages");
    }
}
