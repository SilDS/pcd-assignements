package Ex1_GOL.msg;

import akka.actor.ActorRef;

public class ConfigWorker {

    public boolean[] row;
    public ActorRef upperActor;
    public ActorRef lowerActor;

    public ConfigWorker(boolean[] row, ActorRef up, ActorRef lo) {
        this.row = row;
        this.upperActor = up;
        this.lowerActor = lo;
    }
}
