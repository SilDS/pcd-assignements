package Ex1_GOL;

import akka.actor.ActorSystem;
import akka.actor.Props;

public class Main {

    private static final int gridWidth = 3000, gridHeight = 3000, frameRate = 100;
    private static final int viewWidth = 800, viewHeight = 600;

    public static void main(String[] args) {
        ActorSystem system = ActorSystem.create("GameOfLife");
        system.actorOf(Props.create(BootActor.class, gridWidth, gridHeight, frameRate, viewWidth, viewHeight));
    }
}
