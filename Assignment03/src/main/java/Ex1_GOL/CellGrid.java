package Ex1_GOL;

import java.awt.*;
import java.util.ArrayList;
import java.util.Random;

public class CellGrid {

    private final int width, height;
    private final boolean cells[][];

    public CellGrid(int width, int height) {
        this.width = width;
        this.height = height;
        cells = new boolean[height][width];
    }

    public boolean[] getRow(int row) {
        return this.cells[row];
    }

    public void setLive(ArrayList<Point> live) {
        live.stream().forEach(p -> {
            this.cells[p.x][p.y] = true;
        });
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public boolean isAlive(int x, int y) {
        return cells[y][x];
    }

    public void initRandom(int n) {
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                cells[j][i] = false;
            }
        }

        Random rand = new Random(System.currentTimeMillis());
        for (int i = 0; i < n; i++) {
            int x = rand.nextInt(width);
            int y = rand.nextInt(height);
            cells[y][x] = true;
        }
    }

    public void drawGlider(int x, int y) {
        cells[y][x] = false;
        cells[y][(x + 1) % width] = true;
        cells[y][(x + 2) % width] = false;
        cells[(y + 1) % height][x] = false;
        cells[(y + 1) % height][(x + 1) % width] = false;
        cells[(y + 1) % height][(x + 2) % width] = true;
        cells[(y + 2) % height][x] = true;
        cells[(y + 2) % height][(x + 1) % width] = true;
        cells[(y + 2) % height][(x + 2) % width] = true;
    }

    public void drawBlock(int x, int y) {
        drawConf(x, y, new boolean[][]{{false, false, false, false},
                {false, true, true, false},
                {false, true, true, false},
                {false, false, false, false}});
    }

    private void drawConf(int x, int y, boolean[][] conf) {
        for (int i = 0; i < conf.length; i++) {
            for (int j = 0; j < conf[i].length; j++) {
                cells[(y + i) % height][(x + j) % width] = conf[i][j];
            }
        }
    }
}