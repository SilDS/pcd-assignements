package Ex2_Chat;

import Ex2_Chat.msg.*;
import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.event.Logging;
import akka.event.LoggingAdapter;

import java.util.ArrayList;


public class WhitePages extends AbstractActor {

    private final LoggingAdapter log = Logging.getLogger(getContext().getSystem(), this);

    private ArrayList<ActorRef> contacts = new ArrayList<>();
    private ArrayList<Integer> contactsId = new ArrayList<>();
    private int id = 0;
    private int numProc = 100;

    @Override
    public Receive createReceive() {

        return receiveBuilder()
                .match(Subscribe.class, msg -> {
                    if (id < numProc) {
                        log.info("New user in chat");
                        contacts.add(getSender());
                        contactsId.add(id);
                        contacts.get(id).tell(new Init(id), getSelf());
                        for (ActorRef ref : contacts) {
                            ref.tell(new Contacts(contacts, contactsId), getSelf());
                        }
                        id++;
                    } else {
                        log.info("Max number of users in chat, quitting connection with " + getSender());
                        getSender().tell(new MaxNumParticipants(), getSelf());
                    }
                })
                .match(Unsubscribe.class, msg -> {
                    int i = contacts.indexOf(getSender());
                    contactsId.remove(i);
                    contacts.remove(getSender());
                    log.info("User-" + msg.getId() + " left the chat");
                    for (ActorRef ref : contacts){
                        ref.tell(new Contacts(contacts, contactsId), getSelf());
                    }
                })
                .build();
    }
}
