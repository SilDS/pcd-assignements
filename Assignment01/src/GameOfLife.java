import javax.swing.*;

public class GameOfLife {
    private final static int ROWS=5000;
    private final static int COLS=5000;

    public static void main(String[] args) {



        cellMatrix matrix= new cellMatrix(ROWS,COLS);
        matrix.randomInit();
        Controller controller= new Controller(matrix);
        View view= new View(matrix,controller);
        SwingUtilities.invokeLater(() -> {
            view.setVisible(true);
        });

    }

}
