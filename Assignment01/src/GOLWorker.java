import java.util.concurrent.Semaphore;

public class GOLWorker extends Thread {
    private Semaphore semaphore;
    private Semaphore readyUpdate;
    private int initialRow;
    private int finalRow;
    private cellMatrix cell;
    private Controller controller;

    public GOLWorker(int id, Semaphore sem, cellMatrix cell, int inRow, int finRow, Controller controller, Semaphore readyUpdate){
        this.semaphore =sem;
        this.initialRow=inRow;
        this.finalRow=finRow;
        this.cell=cell;
        this.controller=controller;
        this.readyUpdate=readyUpdate;
    }

    @Override
    public void run() {
        try {
            while (controller.getFlag()) {
                long startTime = System.currentTimeMillis();
                semaphore.acquire();
                cell.updateState( this.initialRow, this.finalRow);
                readyUpdate.release();
                System.out.println("Thread time to calculate the new generation: " + (System.currentTimeMillis()-startTime) + " ms");
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}
