package Ex01;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.regex.Pattern;

public class FSRegexFinder {

    public static void main(String[] args){

        if (args.length!=3){
            usage();
            System.exit(1);
        }

        String rootDir = args[0];
        Pattern regex = Pattern.compile(String.format("\\%s", args[1]));
        int depth = Integer.parseInt(args[2]);
        int poolSize = Runtime.getRuntime().availableProcessors() + 1;

        FSTreeService service = new FSTreeService(poolSize);
        try {
            service.compute(Paths.get(rootDir), regex, depth);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void usage(){
        System.out.println("Usage of Ex01.FSRegexFinder: " +
                "java Ex01.FSRegexFinder dir \'regex\' depth\n" +
                "dir = starting directory\n" +
                "\'regex\' = regex to match\n" +
                "depth = deepness of search");
    }
}
