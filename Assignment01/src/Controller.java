import javax.swing.*;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Semaphore;

public class Controller extends Thread {
    private List<Semaphore> threadSemaphore = new ArrayList<>();
    private List<Semaphore> updateReady = new ArrayList<>();
    private cellMatrix cellMatrix;
    private List<GOLWorker> workers = new ArrayList<>();
    volatile boolean flag = false;
    View view;

    public Controller(cellMatrix matrix) {
        this.cellMatrix = matrix;
    }

    @Override
    public void run() {

        try {

            int nThread = Runtime.getRuntime().availableProcessors() + 1;
            int matrixRow = cellMatrix.getRows();
            int threadRows = matrixRow / nThread;
            int count = 0;

            for (int i = 0; i < nThread - 1; i++) {
                threadSemaphore.add(new Semaphore(1));
                updateReady.add(new Semaphore(0));
                workers.add(new GOLWorker(i, threadSemaphore.get(i), cellMatrix, count, count + threadRows, this, updateReady.get(i)));
                workers.get(i).start();
                count += threadRows;
            }

            threadSemaphore.add(new Semaphore(1));
            updateReady.add(new Semaphore(0));
            int size = threadSemaphore.size();

            workers.add(new GOLWorker(size - 1, threadSemaphore.get(size - 1), cellMatrix, count, count + (matrixRow - count), this, updateReady.get(size - 1)));
            workers.get(size - 1).start();

            while (flag) {

                long startTime = System.currentTimeMillis();
                for (Semaphore update : updateReady) {
                    update.acquire();
                }
                view.setLabel();
                cellMatrix.nextGeneration();
                try {
                    SwingUtilities.invokeAndWait(() -> {
                        view.repaint();
                    });
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                }

                for (Semaphore thread : threadSemaphore) {
                    thread.release();
                }
                System.out.println("Time to update the view: " + (System.currentTimeMillis() - startTime) + " ms");
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    public boolean getFlag() {
        return this.flag;
    }

    public void setView(View view){
        this.view=view;
    }
}
