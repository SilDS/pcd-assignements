package Ex2_Chat;

import Ex2_Chat.msg.*;
import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.ActorSelection;

import java.util.ArrayList;


public class ChatActor extends AbstractActor {

    final ActorSelection whitePages = getContext().actorSelection("akka.tcp://Chat-System@127.0.0.1:2552/user/white-pages");
    private ArrayList<ActorRef> users = new ArrayList<>();
    private ArrayList<Integer> usersId = new ArrayList<>();
    private int id;
    private final static int numProc = 1000;
    private MatrixClock matrixClock = new MatrixClock(numProc, users, getSelf());
    private ChatGUI GUI;

    public void postMsg(String msg) {
        for (int i = 0; i < usersId.size(); i++) {
            matrixClock.sendMsg(id, i);
        }
        getSelf().tell(new Message(msg, id, matrixClock), getSelf());
    }

    @Override
    public void preStart() {
        whitePages.tell(new Subscribe(), getSelf());
        this.GUI = new ChatGUI(getSelf());
        this.GUI.display();
    }


    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(Init.class, msg -> {
                    this.id = msg.getId();
                    matrixClock.init();
                })
                .match(Contacts.class, msg -> {
                    this.users = msg.getContacts();
                    this.usersId = msg.getUsersId();
                    matrixClock.setUsers(users);
                })
                .match(Message.class, msg -> {
                    matrixClock.receiveMsg(msg.getMatrix(), msg.getId(), msg.getMsg());
                })
                .match(ViewMsg.class, msg -> {
                    postMsg(msg.getMsg());
                })
                .match(MaxNumParticipants.class, msg -> {
                    System.exit(0);
                })
                .match(Msg.class, msg -> {
                    GUI.newMsg("User-" + msg.getId() + ": " + msg.getMsg());
                })
                .build();
    }

    @Override
    public void postStop() {
        whitePages.tell(new Unsubscribe(id), getSelf());
    }
}
