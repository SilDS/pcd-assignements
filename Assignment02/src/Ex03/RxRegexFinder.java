package Ex03;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.flowables.ConnectableFlowable;


import java.io.BufferedReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RxRegexFinder {

    public static void main(String[] args) {

        String rootDir = args[0];
        Pattern regex = Pattern.compile(String.format("\\%s",args[1]));
        int depth = Integer.parseInt(args[2]);

        final double[] matches = {0};
        final double[] matchedFile = {0};
        final double[] percentage = {0};
        final double[] mean = {0};
        final int[] numFiles = {0};
        AtomicBoolean found = new AtomicBoolean(false);

        Flowable<Path> files = Flowable.create(emitter -> {
            Files.walk(Paths.get(rootDir), depth)
            .filter(Files::isRegularFile)
                    .forEach(match -> {
                            try(BufferedReader reader = Files.newBufferedReader(match)) {
                                numFiles[0]++;
                                String line = null;
                                while ((line = reader.readLine())!=null) {
                                    Matcher matcher = regex.matcher(match.toString());
                                    while (matcher.find()) {
                                        matches[0]++;
                                        found.set(true);
                                    }
                                }
                            } catch (Exception ex){}
                            if (!found.get()){
                                log("No match found in: " + match);
                            }else {
                                matchedFile[0]++;
                                percentage[0] = (matchedFile[0]/numFiles[0])*100;
                                mean[0] = matches[0]/numFiles[0];
                                log("Match found in: " + match +
                                        "\nMatches: " + matches[0] +
                                        "\nPercentage of file with at least one match: " + percentage[0] + "%" +
                                        "\nMean number of matches: " + mean[0]);
                            }
                    });
        }, BackpressureStrategy.BUFFER);

        ConnectableFlowable<Path> hotObservable = files.publish();
        hotObservable.connect();
    }

    private static void log(String msg){
        System.out.println(msg);
    }
}
