package Ex2_Chat;

import Ex2_Chat.msg.Msg;
import akka.actor.ActorRef;

import java.util.ArrayList;
import java.util.List;

public class MatrixClock {

    private int[][] matrix;
    private int id;
    private int numProc;
    private List<BufferedMsg> queue = new ArrayList<>();
    private List<ActorRef> users;
    private String msg;
    private ActorRef senderRef;

    public MatrixClock(int numProc, ArrayList users, ActorRef senderRef) {
        this.numProc = numProc;
        this.matrix = new int[numProc][numProc];
        this.users = users;
        this.senderRef = senderRef;
    }

    public void init() {
        for (int i = 0; i < numProc; i++) {
            for (int j = 0; j < numProc; j++){
                matrix[i][j] = 0;
            }
        }
    }


    public void setUsers(ArrayList users){
        this.users = users;
    }

    public void sendMsg(int sendId, int destId) {
        matrix[sendId][destId]++;
    }

    public boolean checkEligibility(int[][] senderMat) {
        boolean flag = false;
        for (int i = 0; i < numProc; i++){
            if (i != id){
                for (int j = 0; j < numProc; j++){   //eligible
                    if(matrix[i][j] >= senderMat[i][j]) {
                        flag = true;
                    }else{
                        flag = false;
                    }
                }
            }
        }
        return flag;
    }


    public void receiveMsg(int[][] senderMat, int senderId, String msg) {
        
        this.msg = msg;
        if(!queue.isEmpty()) {
            queue.forEach(e -> {
                boolean eligible = checkEligibility(e.getMatrix());
                if(eligible) {
                    users.forEach(t -> t.tell(new Msg(e.getMsg(), e.senderId), senderRef));
                    matrix[e.senderId][id]++;
                    queue.remove(e);
                }
            });
        }
        boolean flag = checkEligibility(senderMat);
        if(flag){
            matrix[senderId][id]++;

            for (ActorRef ref : users) {
                ref.tell(new Msg(msg, senderId), senderRef);
            }

        }else{
            queue.add(new BufferedMsg(this.msg, senderId, senderMat));
        }

    }

    public int[][] getMatrix(){
        return this.matrix;
    }

    public int getId(){
        return this.id;
    }

    private class BufferedMsg {
        private String msg;
        private int senderId;
        private int [][] matrix;

        public BufferedMsg(String msg, int senderId, int[][] matrix) {
            this.msg = msg;
            this.senderId = senderId;
            this.matrix = matrix;
        }

        public String getMsg(){return this.msg;}

        public int getSenderId(){return this.senderId;}

        public int[][] getMatrix(){return this.matrix;}
    }
}
