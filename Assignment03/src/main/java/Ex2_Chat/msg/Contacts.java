package Ex2_Chat.msg;


import akka.actor.ActorRef;

import java.io.Serializable;
import java.util.ArrayList;

public class Contacts implements Serializable {

    private ArrayList<ActorRef> users;
    private ArrayList<Integer> usersId;

    public Contacts(ArrayList users, ArrayList usersId) {
        this.users = users;
        this.usersId = usersId;
    }

    public ArrayList getContacts() {
        return users;
    }

    public ArrayList getUsersId() {
        return usersId;
    }

}
