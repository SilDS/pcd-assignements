package Ex02;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.file.FileSystem;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/*Prova con Verticle piuttosto che utilizzando callback all'interno del main-loop*/

class FileVerticle extends AbstractVerticle {

    private Path file;
    private Pattern regex;

    public FileVerticle(Path file, Pattern regex){
        this.file = file;
        this.regex = regex;
    }

    public void start(){
        FileSystem fs = getVertx().fileSystem();
        Future<Buffer> fut = Future.future();
        fs.readFile(String.valueOf(file), fut.completer());
        fut.setHandler(res -> {
            int matches = 0;
            Matcher matcher = regex.matcher(res.result().toString());
            boolean found = false;
            while (matcher.find()){
                matches++;
                found = true;
            }
            if (!found){
                System.out.println("No match found in: " + file);
            } else {
                System.out.println("Match found in: " + file + "\nwith:" + matches + " matches");
            }
        });
    }
}


public class AsyncWithFuture {

    public static void main(String[] args) throws IOException {
        Vertx vertx = Vertx.vertx();
        String dir = "/home/silvio/";
        Pattern regex = Pattern.compile("\\a");
        List<Path> files = new LinkedList<>();
        Files.walk(Paths.get(dir),2)
                .filter(Files::isRegularFile)
                .collect(Collectors.toCollection(() -> files));

        for (int i=0; i<files.size(); i++) {
            vertx.deployVerticle(new FileVerticle(files.get(i), regex));
        }
        vertx.close();
    }
}