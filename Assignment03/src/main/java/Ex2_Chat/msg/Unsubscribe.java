package Ex2_Chat.msg;

import java.io.Serializable;

public class Unsubscribe implements Serializable {

    private int id;

    public Unsubscribe(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }
}
